//Drone
#include "mikrokopterInps.h"

static double  PITCH_SCALE = 2;         // counts/deg
static double   ROLL_SCALE = 2;         // counts/deg
static double   DYAW_SCALE = 1;         // counts/deg/s
static double THRUST_SCALE = 5.1988;    // counts/N - approximate

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active),
    okto_command_filter(idDrone)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    return;
}

void DroneCommandROSModule::close()
{
    return;
}

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Publishers
    okto_CtrlInput_publisher = n.advertise<mikrokopter_driver::OktoCommand>("Okto_command", 1, true);

    //Subscribers
    ML_autopilot_command_subscriber = n.subscribe("command/low_level", 1, &DroneCommandROSModule::MLAutopilotCommandCallback, this);

    okto_command_filter.open( nIn, moduleName);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void DroneCommandROSModule::MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    mikrokopter_driver::OktoCommand okto_autopilot_command_msg;
    okto_autopilot_command_msg.nick  = (int16_t) (-1.0) * msg.pitch  *  PITCH_SCALE;
    okto_autopilot_command_msg.roll  = (int16_t) (-1.0) * msg.roll   *  ROLL_SCALE;
    okto_autopilot_command_msg.dyaw  = (int16_t) (+1.0) * msg.dyaw   *  DYAW_SCALE;
    okto_autopilot_command_msg.gas   = (int16_t) (+1.0) * msg.thrust *  THRUST_SCALE;

    KEEP_IN_RANGE( okto_autopilot_command_msg.nick,  -127, +127)   // rango de trabajo: -127...+127
    KEEP_IN_RANGE( okto_autopilot_command_msg.roll,  -127, +127)   // rango de trabajo: -127...+127
    KEEP_IN_RANGE( okto_autopilot_command_msg.dyaw,  -127, +127)   // rango de trabajo: -127...+127
    KEEP_IN_RANGE( okto_autopilot_command_msg.gas,      0,  255)   // rango de trabajo:    0...+255

    // This function call adds, taking into account configuration:
    //    - command saturation
    //    - ctrl mask (enabling of control channels)
    okto_command_filter.assembleCtrlCommands( okto_autopilot_command_msg);

    okto_CtrlInput_publisher.publish( okto_autopilot_command_msg );

    return;
}
