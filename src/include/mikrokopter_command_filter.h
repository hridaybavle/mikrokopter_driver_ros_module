#ifndef OKTO_COMMAND_FILTER_H
#define OKTO_COMMAND_FILTER_H

#include <ros/ros.h>
#include <mikrokopter_driver/OktoCommand.h>
#include "xmlfilereader.h"

class OktoCommandFilter
{
private:
    // Configuration parameters
    // enabling of okto control channels
    bool enable_ctrl_roll_;
    bool enable_ctrl_pitch_;
    bool enable_ctrl_yaw_;
    // saturation of okto commands (min/max output - in asctec units)
    int max_ctrl_thrust_;
    int min_ctrl_thrust_;
    int max_ctrl_roll_;
    int max_ctrl_pitch_;
    int max_ctrl_yaw_;

private:
    void initializeParams(int droneId);

public:
    OktoCommandFilter(int droneId);
    void open(ros::NodeHandle & nIn, std::string moduleName);

    void assembleCtrlCommands(mikrokopter_driver::OktoCommand &msg);
};

#endif // OKTO_COMMAND_FILTER_H
